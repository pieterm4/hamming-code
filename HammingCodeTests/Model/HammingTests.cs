﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HammingCode.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HammingCode.Model.Tests
{
    [TestClass()]
    public class HammingTests
    {
       

        [TestMethod()]
        public void MakeGoodTableTest()
        {
            Hamming hamming = new Hamming("1011");


            //Assert.AreEqual(8, hamming.TebleSize);
            Assert.AreEqual("0110011", hamming.Output);

            Hamming hamming2 = new Hamming("10011010");


            //Assert.AreEqual(8, hamming.TebleSize);
            Assert.AreEqual("011100101010", hamming2.Output);

        }

        [TestMethod()]
        public void TestTest()
        {
            Hamming hamming = new Hamming("10011010");

            Assert.AreEqual("011100101010", hamming.Output);

            hamming.VeryficationCode = "01110010111";
            hamming.GetErrorIndexes();
            Assert.AreEqual(true, hamming.isCorrect);


        }
    }
}