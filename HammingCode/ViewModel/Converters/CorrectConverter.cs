﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace HammingCode.ViewModel.Converters
{
    public class CorrectConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if ((bool) value == true)
                {
                    return "Correct!";
                }
                else
                {
                    return "Wrong!";
                }
            }
            else
            {
                return string.Empty;
            }
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
