﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace HammingCode.ViewModel.Converters
{
    public class WrongBitConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int) value == -1)
            {
                return "-";
            }
            else
            {
                return value.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
