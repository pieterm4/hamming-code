﻿using System;
using System.Linq;
using System.Windows;
using HammingCode.Model;
using HammingCode.ViewModel.Command;

namespace HammingCode.ViewModel
{
    public class MainWindowViewModel:ViewModelBase
    {
        private string _inputCode;
        private string _verificationCode;
        private string _outputHammingCode;
        private bool? _isCorrect;
        private int _wrongBitPossition;
        private string _imagePath;
        private string _correctedCode;

        public string InputCode
        {
            get { return _inputCode; }
            set
            {
                if (Equals(value, _inputCode)) return;
                _inputCode = value;
                HammingCode();
                OnPropertyChanged(nameof(InputCode));
            }
        }

        public string VerificationCode
        {
            get { return _verificationCode; }
            set
            {
                if (Equals(value, _verificationCode)) return;
                _verificationCode = value;
                VerifyCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(VerificationCode));
            }
        }

        public string OutputHammingCode
        {
            get { return _outputHammingCode; }
            set
            {
                if (Equals(value, _outputHammingCode)) return;
                _outputHammingCode = value;
                OnPropertyChanged(nameof(OutputHammingCode));
            }
        }

        public bool? IsCorrect
        {
            get { return _isCorrect; }
            set
            {
                if (Equals(value, _isCorrect)) return;
                _isCorrect = value;
                OnPropertyChanged(nameof(IsCorrect));
            }
        }

        public int WrongBitPossition
        {
            get { return _wrongBitPossition; }
            set
            {
                if(Equals(value, _wrongBitPossition)) return;
                _wrongBitPossition = value;
                OnPropertyChanged(nameof(WrongBitPossition));
            }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                if (Equals(value, _imagePath)) return;
                _imagePath = value;
                OnPropertyChanged(nameof(ImagePath));
            }
        }

        public string CorrectedCode
        {
            get { return _correctedCode; }
            set
            {
                if (Equals(value, _correctedCode)) return;
                _correctedCode = value;
                OnPropertyChanged(nameof(CorrectedCode));
            }
        }


        private void HammingCode()
        {
            Hamming hamming = new Hamming();
            if (!string.IsNullOrEmpty(InputCode))
            {
                hamming.Input = InputCode;
                hamming.MakeGoodTable();
                OutputHammingCode = hamming.Output;
            }
            else
            {
                OutputHammingCode = string.Empty;
            }
            
        }

        public CommandBase VerifyCommand { get; set; }
        public CommandBase SaveCommand { get; set; }
        public CommandBase LoadCommand { get; set; }
        

        public MainWindowViewModel()
        {
            VerifyCommand = new CommandBase(VerifyCommandExecute, VerifyCommandCanExecute);
            SaveCommand = new CommandBase(SaveCommandExecute, o => true);
            LoadCommand = new CommandBase(LoadCommandExecute, o => true);
            IsCorrect = null;
            WrongBitPossition = -1;
        }

        private async void LoadCommandExecute(object obj)
        {
            var inputs = await LoadSaveFile.Load();

            if (inputs.Any())
            {
                InputCode = inputs.ElementAt(0);
                VerificationCode = inputs.ElementAt(1);
            }
        }

        private async void SaveCommandExecute(object obj)
        {
            var verify = "";
            if (IsCorrect != null)
            {
                if (IsCorrect == true)
                {
                    verify = "Correct!";
                }
                else
                {
                    verify = "Wrong!";
                }
            }
            else
            {
                verify = string.Empty;
            }

            var bitpos = "";
            if (WrongBitPossition == -1)
            {
                bitpos = "-";
            }
            else
            {
                bitpos = WrongBitPossition.ToString();
            }

            await LoadSaveFile.Save(InputCode, OutputHammingCode, VerificationCode, verify, bitpos, CorrectedCode);
            MessageBox.Show("Report has been saved", "Saved", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private async void VerifyCommandExecute(object obj)
        {
            Hamming hamming = new Hamming();
            hamming.VeryficationCode = VerificationCode;
            var errorIndex = await hamming.GetErrorIndexes();
            IsCorrect = hamming.isCorrect;

            if (IsCorrect == null)
            {
                WrongBitPossition = -1;
                ImagePath = string.Empty;
            }
            else if (IsCorrect == true)
            {
                WrongBitPossition = -1;
                CorrectedCode = "-";
                ImagePath = @"~\..\Icons\Correct.png";
            }
            else
            {
                if (errorIndex.Any())
                {
                    WrongBitPossition = errorIndex.Sum() + errorIndex.Count;
                    ImagePath = @"~\..\Icons\Wrong.png";
                    hamming.CorrectCode(WrongBitPossition-1);
                    if (string.IsNullOrEmpty(hamming.CorrectedCode))
                    {
                        CorrectedCode = "-";
                    }
                    else
                    {
                        CorrectedCode = hamming.CorrectedCode;
                    }
                   

                }
                else
                {
                    WrongBitPossition = -1;
                    ImagePath = @"~\..\Icons\Wrong.png";
                }
                
            }

            

        }

        private bool VerifyCommandCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(VerificationCode))
            {
                
                return true;
            }
            else
            {
                IsCorrect = null;
                CorrectedCode = "-";
                ImagePath = string.Empty;
                WrongBitPossition = -1;
                return false;
            }
        }
    }
}
