﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Gat.Controls;

namespace HammingCode
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("Do you really wanna close this app?", "Exit", MessageBoxButton.YesNo,
                MessageBoxImage.Question);

            if (dialogResult == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.Title = "Hamming Code";
            about.Copyright = "Copyright 2017 - Piotr Makowiec and Tomasz Krzywonos";
            about.ApplicationLogo = new BitmapImage(new Uri("/Icons/icon.png", UriKind.Relative));

            about.Description =
                "Application has been written for SOB laboratory classes.\nThis desktop app has been writen in C# language as WPF Application using MVVM pattern.\n\nAll rights reserved.";

            about.AdditionalNotes =
                "Enter binary code and this app automatically will compute hamming code.\nYou can also write the binary code for verification, if that code is correct.\nClick verify button, to see if your code is correct, if it doesn't, application shows you where is the mistake and corrects your one wrong bit.\nThis app lets you load data from txt file(first line - code to change into hamming code, second line - verification code).\nYou can also save the report into pdf file.";

            about.Show();
        }
    }
}
