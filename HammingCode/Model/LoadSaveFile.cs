﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Documents;
using System.Windows.Media.Animation;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Win32;
using Paragraph = iTextSharp.text.Paragraph;

namespace HammingCode.Model
{
    public class LoadSaveFile
    {
        public static async Task Save(string inputBinaryCode, string hammingCode, string verifyCode, string verifyStatus, string wrongBitPosition, string correctedCode)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "PDF Files|*.pdf";
            dialog.FilterIndex = 0;

            string fileName = string.Empty;

            if (dialog.ShowDialog() == true)
            {
                fileName = dialog.FileName;
                Document pdfDoc = new Document(PageSize.A4, 50.0f, 50.0f, 50.0f, 10.0f);
                PdfWriter myPdfWriter = PdfWriter.GetInstance(pdfDoc, new FileStream(fileName, FileMode.Create));

                pdfDoc.Open();

                /********************** Title *************************/
                var header = FontFactory.GetFont("Segoe UI", 35.0f, BaseColor.BLACK);
                var title = "Hamming Code Report\n\n";
                var titleParagraph = new Paragraph(title, header);
                titleParagraph.Alignment = Element.ALIGN_CENTER;
                /******************************************************/

                /********************** Subtitles *************************/
                var header2 = FontFactory.GetFont("Segoe UI", 17.0f, BaseColor.BLACK);
                var subtitle1 = "Your input binary code: ";
                var subtitle2 = "\n\nGenerated Hamming Code: ";
                var subtitle3 = "\n\nCode to verify: ";
                var subtitle4 = "\n\nVerification status: ";
                var subtitle5 = "\n\nWrong bit position: ";
                var subtitle6 = "\n\nCorrected code: ";



                var inputParagraph = new Paragraph(subtitle1, header2);
                inputParagraph.PaddingTop = 50.0f;
                var hammingCodeParagraph = new Paragraph(subtitle2, header2);
                hammingCodeParagraph.PaddingTop = 30.0f;
                var codeToVerifyParagraph = new Paragraph(subtitle3, header2);
                codeToVerifyParagraph.PaddingTop = 30.0f;
                var verificationStatusParagraph = new Paragraph(subtitle4, header2);
                verificationStatusParagraph.PaddingTop = 30.0f;
                var wrongBitPositionParagraph = new Paragraph(subtitle5, header2);
                wrongBitPositionParagraph.PaddingTop = 30.0f;
                var correctedCodeParagraph = new Paragraph(subtitle6, header2);
                correctedCodeParagraph.PaddingTop = 30.0f;
                /**********************************************************/

                /********************** Results from Application *************************/
                var normal = FontFactory.GetFont("Segoe UI", 12.0f, BaseColor.BLACK);
                var inputParagraphText = new Paragraph(inputBinaryCode, normal);
                var hammingCodeParagraphText = new Paragraph(hammingCode, normal);
                var codeToVerifyParagraphText = new Paragraph(verifyCode, normal);
                var verificationStatusParagraphText = new Paragraph(verifyStatus, normal);
                var wrongBitPositionParagraphText = new Paragraph(wrongBitPosition, normal);
                var correctedCodeParagraphText = new Paragraph(correctedCode, normal);
                /*************************************************************************/



                

                /********************** Create whole document *************************/
                pdfDoc.Add(titleParagraph);
                pdfDoc.Add(inputParagraph);
                pdfDoc.Add(inputParagraphText);
                pdfDoc.Add(hammingCodeParagraph);
                pdfDoc.Add(hammingCodeParagraphText);
                pdfDoc.Add(codeToVerifyParagraph);
                pdfDoc.Add(codeToVerifyParagraphText);
                pdfDoc.Add(verificationStatusParagraph);
                pdfDoc.Add(verificationStatusParagraphText);
                pdfDoc.Add(wrongBitPositionParagraph);
                pdfDoc.Add(wrongBitPositionParagraphText);
                pdfDoc.Add(correctedCodeParagraph);
                pdfDoc.Add(correctedCodeParagraphText);


                pdfDoc.Close();

               
            }
               

        }

        public static async Task<List<string>> Load()
        {
            List<string> loadedValues = new List<string>();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == true)
            {
                var firstLine = File.ReadAllLines(openFileDialog.FileName);
                foreach (var s in firstLine)
                {
                    loadedValues.Add(s);
                }
            }

            return loadedValues;
        }
    }
}
