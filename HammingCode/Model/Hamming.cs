﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HammingCode.Model
{
    public class Hamming
    {
        public string Input { get; set; }
        public String Output { get; set; }
        public bool isCorrect { get; set; }
        public string VeryficationCode { get; set; }
        public string CorrectedCode { get; set; }
        public int TebleSize { get; set; } // Do usuniecia
        public int[] Table { get; set; }


        public Hamming(string _input)
        {
            this.Input = _input;
            MakeGoodTable();

        }

        public Hamming()
        {
            
        }
       

        //Checking if the number of 1 is even
        private bool isEven(List<int> _numbers)
        {
            int counter = 0;
            for (int i = 1; i < _numbers.Count; i++)
            {
                if (_numbers[i] == 1)
                    counter++;
            }

            if (counter % 2 == 0)
            {
                return true;
            }
            else
                return false;
        }

        private int GetPower(string input)
        {
            int power = 0;
            for (int i = 0; i < Int32.MaxValue; i++)
            {
                if (Math.Pow(2.0, (double)i) > input.Length)
                {
                    power = i;
                    break;
                }
            }

            return power;
        }

        public void MakeGoodTable()
        {

            int power = GetPower(Input);

            int tableSize = Input.Length + power;
            TebleSize = tableSize;
            int[] table = new int[tableSize];

            //Initialize table
            for (int i = 0; i < power; i++)
            {
                table[(int)Math.Pow(2.0, (double)i) - 1] = -1;
            }

            //Insert rest numbers
            int counter = 0;
            for(int i = 0; i<tableSize; i++)
            {
                
                if(table[i] != -1 && counter < Input.Length)
                {
                    table[i] = int.Parse(Input[counter].ToString());
                    counter++;
                }
               
            }
           

            int k = 0;
            while (k < power)
            {
                int i = (int) Math.Pow(2.0, (double) k) - 1;
                int pow = i + 1;
                List<int> countOnes = new List<int>();
                int j = i;

                int flag = 0;
                while (j < tableSize)
                {
                    if (flag == 0)
                    {
                        int count = 0;

                        while (count < pow && j < tableSize)
                        {
                            if (count == pow - 1)
                            {
                                countOnes.Add(table[j]);
                                flag = 1;
                                count++;
                            }
                            else
                            {
                                countOnes.Add(table[j]);
                                count++;
                                j++;
                            }
                        }
                    }
                    else
                    {
                        j += pow + 1;
                        flag = 0;
                    }
                }


                if (isEven(countOnes))
                {
                    table[i] = 0;
                }
                else
                {
                    table[i] = 1;
                }

                k++;
            }

            for (int i = 0; i < tableSize; i++)
            {
                Output += table[i];
            }

        }

        private bool isPowerOfTwo(int number)
        {
            if (number == 0)
            {
                return true;
            }
            else
            {
                if ((number & (number - 1)) == 0)
                {
                    return true;

                }
                return false;
            }
        }

        public async Task<List<int>> GetErrorIndexes()
        {
            isCorrect = true;
            List<int> indexes = new List<int>();
            int k = 0;
            while ((int) Math.Pow(2.0, (double) k) < VeryficationCode.Length)
            {
                int i = (int)Math.Pow(2.0, (double)k) - 1;
                int pow = i + 1;
                List<int> countOnes = new List<int>();
                int j = i;

                int flag = 0;
                while (j < VeryficationCode.Length)
                {
                    if (flag == 0)
                    {
                        int count = 0;

                        while (count < pow && j < VeryficationCode.Length)
                        {
                            if (count == pow - 1)
                            {
                                countOnes.Add(int.Parse(VeryficationCode[j].ToString()));
                                flag = 1;
                                count++;
                            }
                            else
                            {
                                countOnes.Add(int.Parse(VeryficationCode[j].ToString()));
                                count++;
                                j++;
                            }
                        }
                    }
                    else
                    {
                        j += pow + 1;
                        flag = 0;
                    }
                }

                int tmp = -1;
                if (isEven(countOnes))
                {
                    tmp = 0;
                }
                else
                {
                    tmp = 1;
                }

                if (int.Parse(VeryficationCode[i].ToString()) != tmp)
                {
                    indexes.Add(i);
                    isCorrect = false;
                }

                k++;


            }

            return indexes;

        }

        public void CorrectCode(int position)
        {
            var tmp = VeryficationCode.ToCharArray();

            if (position > -1 && position < VeryficationCode.Length)
            {
                if (tmp[position] == '0')
                {
                    tmp[position] = '1';
                }
                else
                {
                    tmp[position] = '0';
                }
            }

            CorrectedCode = new string(tmp);

        }


    }
}
